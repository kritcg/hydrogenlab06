package com.hydrogendev.lab;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public int[] sort(int[] arr) {

        for(int i = arr.length - 1; i >= 0; i--){
            int maxIndex = i;
            for (int j = i; j>=0;j--){
                if(arr[j] > arr[maxIndex])
                    maxIndex = j;
            }
            swap(arr,i,maxIndex);
        }
        return arr;
    }
    public void swap(int[] arr, int indexA, int indexB){
        int temp = arr[indexA];
        arr[indexA] = arr[indexB];
        arr[indexB] = temp;
    }
}
