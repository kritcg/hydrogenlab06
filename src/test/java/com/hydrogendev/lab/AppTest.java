package com.hydrogendev.lab;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Random;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


@RunWith(JUnitParamsRunner.class)
public class AppTest {

    App app = new App();

    @Test
    public void testSorting() throws Exception {
        int[] arr = {5,2,3,1,4};
        assertThat(app.sort(arr), is(new int[]{1,2,3,4,5}));

        int[] randomArr = new Random().ints(100_000,0,100_000).toArray();
        int[] sortedArr = app.sort(randomArr);
        Arrays.sort(randomArr);
        assertThat(sortedArr,is(randomArr));
    }

    @Test
    public void testSwap() throws Exception {
        int[] arr = {2,1};
        app.swap(arr,0,1);
        assertThat(arr,is(new int[]{1,2}));

    }
}
